let palabra1 = "Hola";
let palabra2 = "Chau";
let frase = "";


function test_A_palabra_1() {
    document.write(palabra1);
}

function test_A_palabra_2() {
    document.write(palabra2);
}

function test_A_frase() {
    document.write(frase = palabra1 + " Mundo");
}

function test_B_palabra_1() {
    document.write(palabra1 = palabra2);
}

function test_B_palabra_2() {
    document.write(palabra2 = palabra1);
}

function test_B_frase() {
    document.write(frase = "frase nueva");
}

function test_C_palabra_1() {
    document.write(palabra1 = palabra1 + " " + palabra2);
}

function test_C_palabra_2() {
    document.write(palabra2 = palabra2 + " " + palabra2);
}

function test_C_frase() {
    document.write(frase = palabra2 + " y " + palabra2);
}

function test_D_palabra_1() {
    document.write(palabra1 = "1+1");
}

function test_D_palabra_2() {
    document.write(palabra2 = "2/0");
}

function test_D_frase() {
    document.write(frase = palabra1 + "=2");
}